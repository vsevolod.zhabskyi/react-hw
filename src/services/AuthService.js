import { $authHost, $host } from './index';

export default class AuthService {
	static async registration(user) {
		try {
			const { data } = await $host.post('register', user);

			return data.successful;
		} catch (e) {
			alert(e.response.data.errors.join('\n'));
		}
	}

	static async login(user) {
		try {
			const { data } = await $host.post('login', user);

			const token = data.result.split(' ')[1];

			localStorage.setItem('token', token);

			const userInfo = await this.getProfileInfo();

			return {
				name: data.user.name,
				email: data.user.email,
				token: token,
				role: userInfo.role,
			};
		} catch (e) {
			alert(e.response.data.errors.join('\n'));
		}
	}

	static async logout() {
		try {
			await $authHost.delete('logout');
		} catch (e) {
			console.log(e.response);
		}
	}

	static async getProfileInfo() {
		try {
			if (!localStorage.getItem('token')) return {};

			const { data } = await $authHost.get('users/me');

			return data.result;
		} catch (e) {
			// console.log(e.response);
		}
	}
}
