import React from 'react';
import CourseCard from '../CourseCard/CourseCard';

function CourseList({ courses }) {
	return (
		<div data-testid='course-list'>
			{courses.map((course) => (
				<CourseCard key={course.id} course={course} />
			))}
		</div>
	);
}

export default CourseList;
