import React from 'react';
import { Col } from 'react-bootstrap';
import TruncateString from 'react-truncate-string/dist/truncateString';
import { pipeDuration } from '../../../../helpers/pipeDuration';
import { pipeDate } from '../../../../helpers/pipeDate';
import MyButton from '../../../../common/MyButton/MyButton';
import { useNavigate } from 'react-router-dom';
import { COURSES_ROUTE, UPDATE_COURSE_ROUTE } from '../../../../routeConstants';
import { useDispatch, useSelector } from 'react-redux';
import { deleteCourse } from '../../../../store/courses/actionCreators';
import { useAdmin } from '../../../../hooks/useAdmin';
import { getAuthors } from '../../../../store/selectors';

function CourseCard({ course }) {
	const authors = useSelector(getAuthors);
	const courseAuthors = authors.filter((author) =>
		course.authors.includes(author.id)
	);
	const isAdmin = useAdmin();

	const navigate = useNavigate();
	const dispatch = useDispatch();

	const handleDeleteCourse = () => {
		dispatch(deleteCourse(course.id));
	};

	const editCourse = () => {
		navigate(`${UPDATE_COURSE_ROUTE}/${course.id}`);
	};

	const openCourse = () => {
		navigate(`${COURSES_ROUTE}/${course.id}`);
	};

	return (
		<div className='d-flex p-4 mb-4 border' data-testid='course-list__item'>
			<Col className='col-8 p-0 text-left'>
				<h3>{course.title}</h3>
				<p className='m-0'>{course.description}</p>
			</Col>
			<Col className='col-4 p-2 '>
				<div className='d-flex'>
					<p>
						<b>Authors:&nbsp;</b>
					</p>
					<TruncateString
						text={courseAuthors.map((author) => author.name).join(', ')}
						truncateAt={100}
					/>
				</div>
				<p>
					<b>Duration: </b>
					{pipeDuration(course.duration)} hours
				</p>
				<p>
					<b>Created: </b>
					{pipeDate(course.creationDate)}
				</p>
				<div className='d-flex justify-content-center'>
					<MyButton className='mx-1' buttonText='Open' onClick={openCourse} />
					{isAdmin && (
						<>
							<MyButton
								className='mx-1'
								buttonText='Delete'
								onClick={handleDeleteCourse}
							/>
							<MyButton
								className='mx-1'
								buttonText='Edit'
								onClick={editCourse}
							/>
						</>
					)}
				</div>
			</Col>
		</div>
	);
}

export default CourseCard;
