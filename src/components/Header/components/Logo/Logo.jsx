import logo from './logo.png';

function Logo(props) {
	return <img src={logo} alt='Courses' style={{ width: '130px' }} />;
}

export default Logo;
